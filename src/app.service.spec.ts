import { Test, TestingModule } from '@nestjs/testing';
import { AppService } from './app.service';

describe('AppService', () => {
  let appService: AppService;

  beforeEach(async () => {
    const app: TestingModule = await Test.createTestingModule({
      providers: [
        AppService,
      ],
    }).compile();

    appService = app.get<AppService>(AppService);
  });

  describe('#getHello()', () => {

    it('should return "Hello World!"', () => {
      const hello = appService.getHello();

      expect(hello).toBe('Hello World!');
    });

  });

  describe('#getHelloAsync()', () => {

    it('should resolves to "Hello World!"', async () => {
      const whenDone = appService.getHelloAsync();

      await expect(whenDone).resolves.toBe('Hello World!');
    });

  });

});
